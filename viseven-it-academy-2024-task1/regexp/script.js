let text = "Lorem ipsum dolor sit amet, consectetur vue adipisicing elit. Nihil ipsa Vue sunt, voluptas odit itaque deleniti vUe voluptatum cumque accusamus. Impedit, doloremque.";

let regex = /\bVue\b/g;
let matches = text.match(regex);

console.log(matches);


function validateEmail(email) {
    let regex = /^[a-zA-Z][a-zA-Z0-9]*@[a-zA-Z]+\.[a-zA-Z]+$/;
    return regex.test(email);
  }
  
  console.log(validateEmail("example@text.com")); // true
  console.log(validateEmail("4554example@text.com")); // false
  console.log(validateEmail("example@com")); // false
  console.log(validateEmail("example-test.com")); // false

  
  
let mask = "+##(###)###-##-##";
let number = "380632298388";

let result = mask.replace(/\d/g, match => number.charAt(0));
for (let i = 0; i < number.length; i++) {
  result = result.replace("#", number.charAt(i));
}

console.log(result);

  
