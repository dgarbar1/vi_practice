//Function Declaration
function multiply(a, b, c) {
    const result = a * b * c;
    console.log(result);
}

multiply(2, 3, 4); 


//Function Expression
const multiplyEXP = function(a, b, c) {
    const result = a * b * c;
    console.log(result);
};

multiplyEXP(5, 6, 7);


//Arrow Function
const multiplyARR = (a, b, c) => {
    const result = a * b * c;
    console.log(result);
};

multiplyARR(8, 9, 8);


//Class Function
class Calculator {
    multiplycalc(a, b, c) {
        const result = a * b * c;
        console.log(result);
    }
}

const calculator = new Calculator();
calculator.multiplycalc(7, 6, 5);


function generateMultiplicationTables(number, limit) {
    document.write("<table>");
    for (let x = 1; x <= limit; x++) {
        document.write("<tr>");
        for (let y = 1; y <= limit; y++) {
            document.write(`<td>${x * y * number}</td>`);
        }
        document.write("</tr>");
    }
    document.write("</table>");
}

// Виклик функції для таблиці множення на 10
generateMultiplicationTables(1, 10);

// Виклик функції для таблиці множення на 15
generateMultiplicationTables(1, 15);

// Виклик функції для таблиці множення на 20
generateMultiplicationTables(1, 20);
