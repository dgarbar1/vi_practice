const users = [
    {
      name: "Admin",
      yearOfBirth: 1992,
      role: "admin",
      permission: ["write", "read", "update", "delete"]
    },
    {
      name: "User1",
      yearOfBirth: 1999,
      role: "moderator",
      permission: ["write", "read", "update"]
    },
    {
      name: "User2",
      yearOfBirth: 2003,
      role: "client",
      permission: ["write", "read"]
    },
    {
      name: "User3",
      yearOfBirth: 2001,
      role: "guest",
      permission: ["read"]
    },
    {
        name: "User4",
        yearOfBirth: 2004,
        role: "client",
        permission: ["read"]
    },
    {
        name: "User5",
        yearOfBirth: 1999,
        role: "client",
        permission: ["read"]
    },
    {
        name: "User6",
        yearOfBirth: 1972,
        role: "guest",
        permission: ["read"]
    },
    {
        name: "User7",
        yearOfBirth: 2003,
        role: "moderator",
        permission: ["write", "read", "update"]
    },
    {
        name: "User8",
        yearOfBirth: 2007,
        role: "guest",
        permission: ["read"]
    },
    {
        name: "User9",
        yearOfBirth: 2005,
        role: "moderator",
        permission: ["write", "read", "update"]
    },
    {
        name: "User10",
        yearOfBirth: 2000,
        role: "guest",
        permission: ["read"]
    },
    
   
  ];
  
  
  const currentYear = new Date().getFullYear();
  const ages = users.map(user => currentYear - user.yearOfBirth);
  
  console.log("Ages:", ages);
  
  
  const sortedUsers = users.slice().sort((a, b) => a.yearOfBirth - b.yearOfBirth);
  
  console.log("Sorted Users:", sortedUsers);
  

  const usersUnder21 = users.filter(user => currentYear - user.yearOfBirth < 21);
  
  console.log("Users Under 21:", usersUnder21);
  
  
  const moderatorUsers = users.filter(user => user.role === "moderator");
  const clientUsers = users.filter(user => user.role === "client");
  const guestUsers = users.filter(user => user.role === "guest");
  const adminUser = users.find(user => user.role === "admin");
  
  console.log("Moderator Users:", moderatorUsers);
  console.log("Client Users:", clientUsers);
  console.log("Guest Users:", guestUsers);
  console.log("Admin User:", adminUser);
  
  
  const usersWithWriteAndUpdate = users.filter(user =>
    user.permission.includes("write") && user.permission.includes("update")
  );
  
  console.log("Users with Write and Update:", usersWithWriteAndUpdate);
  