export class User {
    constructor(name, surname, email, yearOfBirth) {
        this.name = name;
        this.surname = surname;
        this.email = email;
        this.yearOfBirth = yearOfBirth;
    }

    getAge() {
        return new Date().getFullYear() - this.yearOfBirth;
    }

    getFullname() {
        return `${this.name} ${this.surname}`;
    }
}



export class Admin extends User {
    read() {
        console.log(`I'm ${this.getFullname()}. I can read.`);
    }

    write() {
        console.log(`I'm ${this.getFullname()}. I can write.`);
    }

    update() {
        console.log(`I'm ${this.getFullname()}. I can update.`);
    }

    remove() {
        console.log(`I'm ${this.getFullname()}. I can remove.`);
    }
}


export class Moderator extends User {
    read() {
        console.log(`I'm ${this.getFullname()}. I can read.`);
    }

    write() {
        console.log(`I'm ${this.getFullname()}. I can write.`);
    }

    update() {
        console.log(`I'm ${this.getFullname()}. I can update.`);
    }
}



export class Client extends User {
    read() {
        console.log(`I'm ${this.getFullname()}. I can read.`);
    }

    write() {
        console.log(`I'm ${this.getFullname()}. I can write.`);
    }
}



export class Guest extends User {
    read() {
        console.log(`I'm ${this.getFullname()}. I can read.`);
    }
}



const adminUser = new Admin("John", "Doe", "john.doe@example.com", 1990);
const moderatorUser = new Moderator("Jane", "Smith", "jane.smith@example.com", 1985);
const clientUser = new Client("Alice", "Johnson", "alice.johnson@example.com", 2000);
const guestUser = new Guest("Bob", "Brown", "bob.brown@example.com", 1995);


adminUser.read();
adminUser.write();
adminUser.update();
adminUser.remove();

moderatorUser.read();
moderatorUser.write();
moderatorUser.update();

clientUser.read();
clientUser.write();

guestUser.read();
