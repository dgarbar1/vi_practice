class User {
    constructor(name) {
        this.name = name;
    }
}

class Admin extends User {
    toString() {
        return `Admin ${this.name}`;
    }
}

class Moderator extends User {
    toString() {
        return `Moderator ${this.name}`;
    }
}

class Client extends User {
    toString() {
        return `Client ${this.name}`;
    }
}

class Guest extends User {
    toString() {
        return `Guest ${this.name}`;
    }
}

class UserFactory {
    static createUser(permission, name) {
        if (permission.includes("remove")) {
            return new Admin(name);
        } else if (permission.includes("update")) {
            return new Moderator(name);
        } else if (permission.includes("write")) {
            return new Client(name);
        } else if (permission.includes("read")) {
            return new Guest(name);
        } else {
            throw new Error("Invalid permission");
        }
    }
}


const permission1 = ["read", "write", "update", "remove"];
const permission2 = ["read", "write", "update"];
const permission3 = ["read", "write"];
const permission4 = ["read"];

const userFactory = new UserFactory();

const user1 = UserFactory.createUser(permission1, "AdminUser");
const user2 = UserFactory.createUser(permission2, "ModeratorUser");
const user3 = UserFactory.createUser(permission3, "ClientUser");
const user4 = UserFactory.createUser(permission4, "GuestUser");

console.log(user1.toString()); 
console.log(user2.toString());  
console.log(user3.toString()); 
console.log(user4.toString());  
