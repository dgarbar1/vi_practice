//1
var userObject = {
  name: "User1",
  yearOfBirth: 1999,
  role: "moderator",
  permission: ["write", "read", "update"]
};

var userString = JSON.stringify(userObject);
console.log(userString);

//2
var newUserObject = JSON.parse(userString);
console.log(newUserObject);

//3
userObject.toJSON = function () {
  return {
    name: this.name,
    yearOfBirth: this.yearOfBirth,
  };
};

var jsonString = JSON.stringify(userObject);
console.log(jsonString);

//4
function submitForm() {
    
    const username = document.getElementById('username').value;
    const email = document.getElementById('email').value;
    const message = document.getElementById('message').value;

    
    const formData = {
      username: username,
      email: email,
      message: message
    };

    
    console.log(JSON.stringify(formData));
    alert(200);

    
  }