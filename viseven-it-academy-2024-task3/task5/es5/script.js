function User(name, surname, email, yearOfBirth) {
    this.name = name;
    this.surname = surname;
    this.email = email;
    this.yearOfBirth = yearOfBirth;
}

User.prototype.getAge = function() {
    return new Date().getFullYear() - this.yearOfBirth;
};

User.prototype.getFullname = function() {
    return `${this.name} ${this.surname}`;
};


function Admin(name, surname, email, yearOfBirth) {
    User.call(this, name, surname, email, yearOfBirth);
}

Admin.prototype = Object.create(User.prototype);

Admin.prototype.read = function() {
    console.log(`I'm ${this.getFullname()}. I can read.`);
};

Admin.prototype.write = function() {
    console.log(`I'm ${this.getFullname()}. I can write.`);
};

Admin.prototype.update = function() {
    console.log(`I'm ${this.getFullname()}. I can update.`);
};

Admin.prototype.remove = function() {
    console.log(`I'm ${this.getFullname()}. I can remove.`);
};


function Moderator(name, surname, email, yearOfBirth) {
    User.call(this, name, surname, email, yearOfBirth);
}

Moderator.prototype = Object.create(User.prototype);

Moderator.prototype.read = function() {
    console.log(`I'm ${this.getFullname()}. I can read.`);
};

Moderator.prototype.write = function() {
    console.log(`I'm ${this.getFullname()}. I can write.`);
};

Moderator.prototype.update = function() {
    console.log(`I'm ${this.getFullname()}. I can update.`);
};


function Client(name, surname, email, yearOfBirth) {
    User.call(this, name, surname, email, yearOfBirth);
}

Client.prototype = Object.create(User.prototype);

Client.prototype.read = function() {
    console.log(`I'm ${this.getFullname()}. I can read.`);
};

Client.prototype.write = function() {
    console.log(`I'm ${this.getFullname()}. I can write.`);
};


function Guest(name, surname, email, yearOfBirth) {
    User.call(this, name, surname, email, yearOfBirth);
}

Guest.prototype = Object.create(User.prototype);

Guest.prototype.read = function() {
    console.log(`I'm ${this.getFullname()}. I can read.`);
};


const adminUser = new Admin("John", "Doe", "john.doe@example.com", 1990);
const moderatorUser = new Moderator("Jane", "Smith", "jane.smith@example.com", 1985);
const clientUser = new Client("Alice", "Johnson", "alice.johnson@example.com", 2000);
const guestUser = new Guest("Bob", "Brown", "bob.brown@example.com", 1995);


adminUser.read();
adminUser.write();
adminUser.update();
adminUser.remove();

moderatorUser.read();
moderatorUser.write();
moderatorUser.update();

clientUser.read();
clientUser.write();

guestUser.read();